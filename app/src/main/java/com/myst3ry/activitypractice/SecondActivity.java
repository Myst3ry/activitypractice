package com.myst3ry.activitypractice;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondActivity extends AppCompatActivity {

    private static final String EXTRA_TEXT = "text";

    @BindView(R.id.text)
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        editText.setText(getIntent().getStringExtra(EXTRA_TEXT));
    }

    public static Intent newIntent(Context context, String data) {
        final Intent intent = new Intent(context, SecondActivity.class);
        intent.putExtra(EXTRA_TEXT, data);
        return intent;
    }

    @Override
    public void onBackPressed() {
        final Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_TEXT, editText.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}
