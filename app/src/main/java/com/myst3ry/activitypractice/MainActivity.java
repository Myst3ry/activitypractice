package com.myst3ry.activitypractice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final int RCODE_SEND_TEXT = 99;

    @BindView(R.id.text)
    EditText editTextView;
    @BindView(R.id.button_one)
    Button buttonOne;
    @BindView(R.id.button_two)
    Button buttonTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setListeners();
    }

    public void setListeners() {
        buttonOne.setOnClickListener(new FirstBtnListener());
        buttonTwo.setOnClickListener(new SecondBtnListener());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && requestCode == RCODE_SEND_TEXT && resultCode == RESULT_OK) {
            editTextView.setText(data.getStringExtra(Intent.EXTRA_TEXT));
        }
    }

    private final class FirstBtnListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            startActivity(SecondActivity.newIntent(v.getContext(), editTextView.getText().toString()));
        }
    }

    private final class SecondBtnListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            startActivityForResult(SecondActivity.newIntent(v.getContext(), editTextView.getText().toString()), RCODE_SEND_TEXT);
        }
    }
}
